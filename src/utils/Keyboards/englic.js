const englic = {
  default: [
    "ȁ ȍ ǩ ȃ ȇ ȑ ȉ Ǳ ǽ ǿ",
      "ǡ ȅ ǧ ǫ ǭ ǯ ǳ ǵ Ƿ",
      "{shift} ȓ ȏ ǥ ȋ ǣ ǻ ǹ {backspace}",
      "{numbers} {space} ' {ent}"
  ],
  shift: [
    "Ȁ Ȍ Ǩ Ȃ Ȇ Ȑ Ȉ ǰ Ǽ Ǿ",
      "Ǡ Ȅ Ǧ Ǫ Ǭ Ǯ ǲ Ǵ Ƕ",
      "{shift} Ȓ Ȏ Ǥ Ȋ Ǣ Ǻ Ǹ {backspace}",
    "{numbers} {space} ' {ent}"
  ],
  numbers: ["1 2 3", "4 5 6", "7 8 9", "{abc} 0 {backspace}"]
};

export default englic;
