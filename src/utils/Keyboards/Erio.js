const erio = {
  default: [
    "Q W E R T Y U I O P",
    "@ S D F G H J K L [",
    "A Z X C V B N M {backspace}",
    "{numbers} {space} ' {ent}"
  ],
  shift: [
    "q w e r t y u i o p",
    "a s d f g h j k l",
    "a z x c v b n m {backspace}",
    "{numbers} {space} ' {ent}"
  ],
  numbers: ["1 2 3", "4 5 6", "7 8 9", "{abc} 0 {backspace}"]
};

export default erio;
