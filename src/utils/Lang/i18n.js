import i18n from "i18next";
import { useTranslation, initReactI18next } from "react-i18next";
import lang_info from './lang'

i18n.use(initReactI18next).init({
  // we init with resources
  resources: lang_info,
  lng: "uCLC",
  fallbackLng: "uCLC",
  debug: true,
  // saveMissing: true,
  // have a common namespace used around the full app
  ns: ["translations"],
  defaultNS: "translations",

  keySeparator: false, // we use content as keys

  interpolation: {
    escapeValue: false, // not needed for react!!
    formatSeparator: ","
  },
  react: {
    wait: true
  }
});

export default i18n;
