import erio from '../Keyboards/Erio'
import avestan from '../Keyboards/Avestan'
import englic from '../Keyboards/englic'

let lang_info = {
  CL: {
    translations: {
      mainSearchTitle: "JKL FPNDEFP'[Z GZ IKND[?",
      mainSerchPlaceholder: "BZYZR[N @EJ BZFP...",

      ArticleSerchPlaceholder: "BZFP...",
      ArticleSidebarTitleMain: "LP[Z'[Z CA[Z",
      ArticleSidebarTitleInter: "IKXTEYZO",
      ArticleSidebarTitleHelp: "[AIL[T",
      ArticleSidebarTitleAbout: "JKLBALZ",
      ArticleSidebarTitleLang: "MKBAYIA",

      Advantages: "Advantages",

      KeyboardEnt: "BZFP",
      KeyboardABC: "@CD",

    },
    keyboard : erio,
    showName : 'CALN[T',
    rtl : false,
    unicode: false
  },
  'u@Y': {
    translations: {
      mainSearchTitle: "WHAT ARE YOU LOOKING FOR?",
      mainSerchPlaceholder: "Ȇȑǿǩ ǡǻǧ ȅǩǡȃǥǯ...",

      ArticleSerchPlaceholder: "search...",
      ArticleSidebarTitleMain: "Main Page",
      ArticleSidebarTitleInter: "Interaction",
      ArticleSidebarTitleHelp: "Help",
      ArticleSidebarTitleAbout: "About",
      ArticleSidebarTitleLang: "Langueges",

      Advantages: "Vorteile",

      KeyboardEnt: "ȃǩȇȉȃǻ",
      KeyboardABC: "ǠǢǤ",

    },
    keyboard : englic,
    showName : 'English',
    rtl : false,
    unicode: true
  },
  uCLC: {
    translations: {
      mainSearchTitle: "𐬛𐬀𐬭 𐬘𐬎𐬫𐬆𐬱٬𐬆 𐬗𐬆 𐬵𐬀𐬯𐬙𐬌؟",
      mainSerchPlaceholder: "𐬠𐬆𐬥𐬆𐬬𐬌𐬯. 𐬋. 𐬠𐬆𐬘𐬎...",

      ArticleSerchPlaceholder: "𐬠𐬆𐬘𐬎...",
      ArticleSidebarTitleMain: "𐬭𐬎𐬫𐬆'𐬫𐬆. 𐬞𐬁𐬫𐬆",
      ArticleSidebarTitleInter: "𐬵𐬀𐬨𐬐𐬊𐬥𐬆𐬱",
      ArticleSidebarTitleHelp: "𐬫𐬁𐬵𐬭𐬌",
      ArticleSidebarTitleAbout: "𐬛𐬀𐬭𐬠𐬁𐬭𐬆",
      ArticleSidebarTitleLang: "𐬰𐬀𐬠𐬁𐬥𐬵𐬁",

      Advantages: "Les avantages",

      KeyboardEnt: "𐬠𐬆𐬘𐬎",
      KeyboardABC: "𐬀𐬆𐬰",
    },
    keyboard : avestan,
    showName : '𐬞𐬁𐬭𐬯𐬌𐬐',
    rtl : true,
    unicode: true
  }
}






export function is_rtl(lang) {
  try {
    return lang_info[lang].rtl
  } catch {

    return false
  }
}

export function is_unicode(lang) {
  try {
    return lang_info[lang].unicode
  } catch {
    return false
  }
}

export function setKeyboardLayout(lang) {
  console.log(lang);
  console.log(lang_info[lang]);
  return lang_info[lang].keyboard

}

export function lang_list() {
  return lang_info;
}

export default lang_info
