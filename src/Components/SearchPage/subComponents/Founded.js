import React from 'react';

import Items from './Items'
import {api_server} from '../../../settings'
import {is_rtl, is_unicode} from '../../../utils/Lang/lang'





const Box = (props) => (
    <div className="">


        <div className="searchBox" dangerouslySetInnerHTML={{ __html: props.data.description }} />

    </div>
)





class Founded extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      search_list: [],
      search_best: {},
      is_rtl: (is_rtl(this.props.lang))?"-rtl":""
    };

    // this.getData = this.getData.bind(this)
  }


  componentDidMount(){
    // console.log(this.props);
    // this.setState({
    //   rtl: (props.match.params.lang == 'uCLC')?"":"-rtl"
    // })
    fetch(api_server+'/api/v1/search/', {
        // fetch('http://192.168.43.10:8000/GetEvent/', {
          method: 'POST',
          // mode: 'no-cors',
          headers: {
            Accept: 'application/json',
                    'Access-Control-Allow-Origin':'*',
                    'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            search_keys : this.props.data.key,
            language : this.props.lang
          }),
        }).then((response) => {
              return response.json();
          })
          .then(data => {
            this.setState({
              search_list: data['search']['list'],
              search_best: data['search']['best_article']
            });
          })
          .catch((error) => {

          });
  }



  render() {
    return (
      <div className={"founded"+this.state.is_rtl}>
        <div className="searchGrid" >
            <div>
              {
                this.state.search_list.map((item, i) => (
                  <Items key={i+1} lang={this.props.lang} data={item} />
                ))
              }
            </div>
            {
              (this.state.search_best['title'])?<Box data={this.state.search_best}/>:null
            }

        </div>
      </div>
    );
  }
}


export default Founded
