import React from 'react';

import {NavLink} from "react-router-dom";



const Items = (props)=> (

  <div className="searchItem">
      <NavLink to={props.data.hashtag} className="itemsTitle">
        {props.data.title}
      </NavLink>
      <div className="itemDesc" dangerouslySetInnerHTML={{ __html: props.data.description }} />

  </div>


);


export default Items
