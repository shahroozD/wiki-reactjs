import React from 'react';


class ArticleImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // options: []
    };
  }


  render() {
    return (
      <div className="articleRight">
        <div className="articleRightInner">
          <img src="https://html5-templates.com/demo/wikipedia-template/img/pencil.jpg" alt="pencil" />
        </div>
        This is a blue <a href="">pencil</a>
      </div>
    );
  }
}


export default ArticleImage
