import React, {useState} from 'react';
import queryString from 'query-string'
import { useTranslation } from "react-i18next";
import i18next from 'i18next';
import Header from './subComponents/Header'
import Sidebar from './subComponents/Sidebar'
import Founded from './subComponents/Founded'
import Footer from './subComponents/Footer'
import {is_rtl, is_unicode} from '../../utils/Lang/lang'

import '../../styles/SearchStyle/style.scss';






function SearchPage(props) {
  // let vh = window.innerHeight * 0.01;
  // document.documentElement.style.setProperty('--vh', `${vh}px`;

    let query = queryString.parse(props.location.search)
    let current_lang = i18next.language
    const [dir_lang, setDir_lang] = useState((is_rtl(props.match.params.lang))?"-rtl":"");
    const { t, i18n } = useTranslation();
    if (!!props.match.params.lang) {
      if (i18next.language != props.match.params.lang) {
          i18n.changeLanguage(props.match.params.lang);
          current_lang = props.match.params.lang
      }
    }

    if (is_rtl(props.match.params.lang)) {
      document.body.style.direction = "rtl";
    }
    if (is_unicode(props.match.params.lang)) {
      document.body.style.fontFamily = ['ardeshir', 'roboto'];
    }



    return (
      <div className="wrapAll clearfix">
    			<Sidebar lang={current_lang}/>
    			<div className={"mainsection"+dir_lang}>
            <Header lang={current_lang} data={{key:query.q}}/>
    				<div className="tabs clearfix">
    				</div>
    				<Founded lang={current_lang} data={{key:query.q}}/>
            <Footer />

    			</div>
    		</div>
    );
}


export default SearchPage
