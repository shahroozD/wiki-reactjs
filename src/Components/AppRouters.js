import React from 'react';
import ReactDOM from 'react-dom';
import i18n from "i18next";

import Article from './ArticlePage/ArticlePage';
import SearchPage from './SearchPage/SearchPage';
import MainPage from './MainPage/MainPage';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";




// <Route path="/:lang/search/" component={SearchPage} exact/>

const AppRouters = () => (
  <Router>
      <div>
        <Switch>
          <Route path="/:lang/search/:sid" component={SearchPage} exact/>
          <Route path="/:lang/search" component={SearchPage} exact/>
          <Route path="/" component={MainPage} exact />
          <Route path="/:lang" component={MainPage} exact />
          <Route path="/:lang/:aid" component={Article} exact />

          <Route component={NoMatch} />
        </Switch>
      </div>
    </Router>
  );



function NoMatch() {
  return <h2>404</h2>;
}


export default AppRouters
