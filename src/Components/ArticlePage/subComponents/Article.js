import React from 'react';

import List from './List'
import Section from './Section'
import NavigationBox from './NavigationBox'
import Catlinks from './Catlinks'
import {api_server} from '../../../settings'



class Article extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      article_data: {},
      is_rtl: (this.props.lang == 'uCLC')?"-rtl":""
    };

    // this.getData = this.getData.bind(this)
  }


  componentDidMount(){
    // console.log(props.match);
    // this.setState({
    //   rtl: (props.match.params.lang == 'aeu')?"":"-rtl"
    // })
    fetch(api_server+'/api/v1/article/', {
        // fetch('http://192.168.43.10:8000/GetEvent/', {
          method: 'POST',
          // mode: 'no-cors',
          headers: {
            Accept: 'application/json',
                    'Access-Control-Allow-Origin':'*',
                    'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            articlename : this.props.article_key,
            language : this.props.lang
          }),
        }).then((response) => {
              return response.json();
          })
          .then(data => {
            // console.log(data)
            this.setState({
              article_data: data['article']
            });
          })
          .catch((error) => {

          });
  }



  render() {
    return (
      <div className={"article"+this.state.is_rtl}>
          <div className="articleCover">
              {
                (this.state.article_data.cover_url)? <img src={"/media/"+this.state.article_data.cover_url} alt="" />:null
              }
          </div>

          <h1>{this.state.article_data.title}</h1>


          <div dangerouslySetInnerHTML={{ __html: this.state.article_data.description }} />

          {
            (this.state.article_data.content_list && this.state.article_data.content_list.length)?
                    <List lang={this.props.lang} data={this.state.article_data.content_list}/>
                    :null
          }
          {
            (!!this.state.article_data.content && this.state.article_data.content.length)?
              this.state.article_data.content.map((data, index) => (
                <Section key={index} data={data} />
              )):null
          }

          {
            (null)?
                    <div>
                        <NavigationBox />
                        <Catlinks />
                    </div>
                    :null
          }

      </div>
    );
  }
}


export default Article
