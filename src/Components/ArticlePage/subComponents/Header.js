import React, {useState, useRef} from 'react';
import { useHistory, Redirect } from 'react-router-dom'
import { useTranslation } from "react-i18next";
import { FiSearch } from 'react-icons/fi';
import { FiMenu } from "react-icons/fi";
import { FaRegKeyboard } from 'react-icons/fa';

import Keyboard from 'react-simple-keyboard';
import 'react-simple-keyboard/build/css/index.css';

import {is_rtl, setKeyboardLayout} from '../../../utils/Lang/lang'

function Header(props) {
    let { t } = useTranslation();
    let history = useHistory();
    const keyboard = useRef(null);
    const [input, setInput] = useState('');
    const [shift, setShift] = useState("default");
    const [layout, setLayout] = useState(setKeyboardLayout(props.lang));
    const [showKB, setShowKB] = useState(0);

    const dir_lang = (is_rtl(props.lang))?"-rtl":""



    const onChange = (input) => {
      window.navigator.vibrate(4);
      setInput(input)
      if (shift === "shift") setShift("default");
    }
    const onChangeInput = event => {
      const input = event.target.value;
      console.log('dddddd');
      setInput(input)
      keyboard.current.setInput(input);
      if (shift === "shift") setShift("default");
    };

    const onKeyPress = (button) => {
      if (button === "{ent}" || button === "{enter}") {
        window.navigator.vibrate(4);
        document.forms[0].submit();
      }
      if (button === "{numbers}" || button === "{abc}") handleNumbers();
      if (button === "{shift}" || button === "{lock}") handleShift();
    }

    const handleShift = () => {
      setShift(shift === "default" ? "shift" : "default")
    };
    const handleNumbers = () => {
      setShift(shift === "numbers" ? "default" : "numbers")
    }


    const showKeyboard = () => {
      setShowKB(showKB ? 0 : 1);
      keyboard.current.setInput(input);
    }

    const submit = (e) => {
      if (input) {
        window.navigator.vibrate(4);
        document.forms[0].submit();
      }
    }


    // let is_rtl = (props.lang == 'uCLC')?"-rtl":""

    return (
      <div className="headerLinks">
      <div className={'headeItems'+dir_lang}>
          <a className={'headeBurger'+dir_lang} href="#">
              <FiMenu className='burgerIcon'/>
          </a>
          <a className={'headeMiniLogo'+dir_lang} href={"/"+props.lang}>
            R[JZNDA
          </a>
      </div>
        <form action={"search"} id="simpleSearch">
          <button type="button" className={"btn-keyboard btn-keyboard-dir"+dir_lang } onClick={showKeyboard}>
              <FaRegKeyboard style={{ fill: (showKB)? '#0303b0' : '#6a6969'}} />
          </button>
          <input type="text" value={input} name="q" id="searchInput" className={'searchInput'+dir_lang}
                 placeholder={t('ArticleSerchPlaceholder')} size="12"  onChange={onChangeInput}
                 autoComplete="off" />
          <button type='button' id="submitSearch"  onClick={submit}
                  className={"btn-search btn-search-dir"+dir_lang}>
              <FiSearch />
          </button>
        </form>

        <div className={'absolute-keyboard keyboard_div '+ (!showKB &&'absolute-keyboard_hide')}>
            <Keyboard
              keyboardRef={r => (keyboard.current = r)}
              onChange={onChange}
              onKeyPress={onKeyPress}
              theme= {'hg-theme-default hg-layout-default '+(showKB &&'simple-keyboard_show')}
              layoutName={shift}
              mergeDisplay= {true}
              layout= {layout}
                display= {{
                  "{numbers}": "123",
                  "{ent}": t('KeyboardEnt'),
                  "{backspace}": "⌫",
                  "{shift}": "⇧",
                  "{abc}": t("KeyboardABC")
                }}
            />
        </div>
      </div>
    );
}


export default Header
