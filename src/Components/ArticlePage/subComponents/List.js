import React from 'react';
import { useTranslation } from "react-i18next";


function List(props){

  const { t } = useTranslation();
  let is_rtl = (props.lang == 'uCLC')?"-rtl":""
  const subList = (list, index) =>{
      var title = list[0]
      return(
          <li key={index}>
            <span>{index+1}</span><a href={"#" +title}>{title}</a>
            <ul>
              {
                list.map((sub_item, sub_index) => (
                  (sub_index)?<li key={sub_index}><span>{(index+1)+'.'+(sub_index)}</span><a href={"#"+sub_item}>{sub_item}</a></li>:null
                ))
              }
            </ul>
          </li>
      )
  }



    return (
      <div className={"contentsPanel"+is_rtl}>
        <div className="contentsHeader">CZILZND</div>
        <ul>

          {
            (!!props.data)?props.data.map((item, index) => (
              (Array.isArray(item))?subList(item, index):<li key={index}><span>{index+1}</span><a href={"#"+item}>{item}</a></li>
            )):null
          }

        </ul>
      </div>
    );
}


export default List
