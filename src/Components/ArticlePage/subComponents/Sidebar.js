import React from 'react';
import {NavLink} from "react-router-dom";

import { useTranslation } from "react-i18next";



function Sidebar(props) {

    const { t } = useTranslation();
    let is_rtl = (props.lang == 'uCLC')?"-rtl":""


    return (
      <div className="sidebar">
        <div className="logo">
          <a className="SidebarLogo" href={"/"+props.lang}>
            R[JZNDA
          </a>
        </div>
        <div className="navigation">
          <ul>
            <li><NavLink to={"/"+props.lang}>{t('ArticleSidebarTitleMain')}</NavLink></li>
          </ul>
          <h3>{t('ArticleSidebarTitleInter')}</h3>
          <ul>
            <li><NavLink to="#s">{t('ArticleSidebarTitleHelp')}</NavLink></li>
            <li><NavLink to="#s">{t('ArticleSidebarTitleAbout')}</NavLink></li>
          </ul>
          <h3>{t('ArticleSidebarTitleLang')}</h3>
          <ul>
            <li><NavLink to="#s">CALN[T</NavLink></li>
            <li className="newbidi"><NavLink to="#s">īĜĵīĐİĜĽĖ</NavLink></li>
          </ul>
        </div>


      </div>
    );
}


export default Sidebar
