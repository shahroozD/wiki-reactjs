import React from 'react';
import ArticleImage from './ArticleImage'


class Section extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // options: []
    };
  }


  render() {
    return (
      <div>
          <h2 id={this.props.data.title}>{this.props.data.title}</h2>
          {!!(this.props.data.photos|length) && <ArticleImage />}
          {
            this.props.data.description.map((data) => (
              <div key={data.title}>
                 {!!(data.title) && <h3 id={data.title}>{data.title}</h3>}
                <div dangerouslySetInnerHTML={{ __html: data.description }} />
              </div>
            ))
          }
      </div>

    );
  }
}


export default Section
