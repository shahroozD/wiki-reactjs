import React, {useState} from 'react';
import { useTranslation } from "react-i18next";
import i18next from 'i18next';
import Header from './subComponents/Header'
import Sidebar from './subComponents/Sidebar'
import Article from './subComponents/Article'
import Footer from './subComponents/Footer'

import {is_rtl, is_unicode} from '../../utils/Lang/lang'

import '../../styles/ArticleStyle/style.scss';





function ArticlePage(props) {
    document.body.style.overflow = "inherit"
    let current_lang = i18next.language
    const [dir_lang, setDir_lang] = useState((is_rtl(props.match.params.lang))?"-rtl":"");
    const { t, i18n } = useTranslation();
    if (!!props.match.params.lang) {
      if (i18next.language != props.match.params.lang) {
          i18n.changeLanguage(props.match.params.lang);
          current_lang = props.match.params.lang
      }
    }

    if (is_rtl(props.match.params.lang)) {
      document.body.style.direction = "rtl";
    }
    if (is_unicode(props.match.params.lang)) {
      document.body.style.fontFamily = ['ardeshir', 'roboto'];
    }


    return (
      <div className="wrapAll clearfix">
    			<Sidebar lang={current_lang}/>
    			<div className={"mainsection"+dir_lang}>
            <Header lang={current_lang}/>
    				<div className="tabs clearfix">
    				</div>
            <Article lang={current_lang} article_key={props.match.params.aid}/>
    				<Footer />
    			</div>
    		</div>
    );
}


export default ArticlePage
