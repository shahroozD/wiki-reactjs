import React from 'react';
import {NavLink} from "react-router-dom";
import LittleNoneCover from '../../../img/Photo_by_João_Silas_on_Unsplash.jpg';


function ContentBox(props) {
  // console.log(props.data);
    let is_rtl = (props.lang == 'uCLC')?"-rtl":""
    let lang = (props.lang)?props.lang:"CL"

    return (
      <NavLink to={'/'+lang+'/'+props.data.hashtag} >
          <div className={"contentBox"+is_rtl} >
            <div className="topBox">
              <div className="gradientBox">
                <h2>{props.data.title}</h2>
              </div>
              <div className="imageBox">
                {
                  (props.data.cover_url)? <img src={"/media/"+props.data.cover_url} alt="" />:<img src={LittleNoneCover} alt="" />
                }
              </div>
            </div>
            <div className="bottomBox" dangerouslySetInnerHTML={{ __html: props.data.description }} />
          </div>
      </NavLink>
    );

}
export default ContentBox
