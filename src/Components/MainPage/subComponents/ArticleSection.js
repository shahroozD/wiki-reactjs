import React from 'react';
import ContentBox from './ContentBox';
import {NavLink} from "react-router-dom";
import {api_server} from '../../../settings'
import NoneCover from '../../../img/Photo_by_Jaredd_Craig_on_Unsplash.jpg';




class ArticleSection extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      chosen_article: {},
      articles_list: [],
      is_rtl: (this.props.lang == 'uCLC')?"-rtl":"",
      preStateY: 0,

      data:{
        list:[{
          hashtag: "asd",
          title: "asdsdasdasdasd",
          cover_url: "asdsdasdasdasd",
          description: "asdasdasdasd"
        },
        {
          hashtag: "asd",
          title: "asdsdasdasdasd",
          cover_url: "asdsdasdasdasd",
          description: "DZL[DA NANAY[T, YKWEND[Y OAI'Z @[L[E @KND. @[ TZ IKXAYKYJ'OEJZ @KM YKRZ'[Z [KMJUZLJ @KND, DZL[DA NANAY[T, YKWEND[Y OAI'Z @[L[E @KND. @[ TZ IKXAYKYJ'OEJZ @KM YKRZ'[Z [KMJUZLJ @KNDZL[DA NANAY[T, YKWEND[Y OAI'Z @[L[E @KND. @[ TZ IKXAYKYJ'OEJZ @KM YKRZ'[Z [KMJUZLJ @KND, DZL[DA NANAY[T, YKWEND[Y OAI'Z @[L[E @KND. @[ TZ IKXAYKYJ'OEJZ @KM YKRZ'[Z [KMJUZLJ @KNDZL[DA NANAY[T, YKWEND[Y OAI'Z @[L[E @KND. @[ TZ IKXAYKYJ'OEJZ @KM YKRZ'[Z [KMJUZLJ @KND, DZL[DA NANAY[T, YKWEND[Y OAI'Z @[L[E @KND. @[ TZ IKXAYKYJ'OEJZ @KM YKRZ'[Z [KMJUZLJ @KN"
        }],
        chosen:{
          hashtag: "DZL[DA_NANAY[T",
          title: "DZL[DA NANAY[T",
          cover_url: "asdsdasdasdasd",
          description: "DZL[DA NANAY[T, YKWEND[Y OAI'Z @[L[E @KND. @[ TZ IKXAYKYJ'OEJZ @KM YKRZ'[Z [KMJUZLJ @KND, DZL[DA NANAY[T, YKWEND[Y OAI'Z @[L[E @KND. @[ TZ IKXAYKYJ'OEJZ @KM YKRZ'[Z [KMJUZLJ @KND..."
        }
      }
    };

    this.wheel = this.wheel.bind(this)
    this.swapp = this.swapp.bind(this)
    this.scroll = this.scroll.bind(this)
  }

  nav = React.createRef();

  componentDidMount(){
    // console.log(this.props.match);
    // this.setState({
    //   rtl: (this.props.match.params.lang == 'uCLC')?"":"-rtl"
    // })
    // document.getElementById('scrollable_section').addEventListener('scroll', this.scroll, true);

    fetch(api_server+'/api/v1/home_page/', {
        // fetch('http://192.168.43.10:8000/GetEvent/', {
          method: 'POST',
          // mode: 'no-cors',
          headers: {
            Accept: 'application/json',
                    'Access-Control-Allow-Origin':'*',
                    'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            lang : this.props.lang
          }),
        }).then((response) => {
              return response.json();
          })
          .then(data => {
            // console.log(data)
            this.setState({
              chosen_article: data['home_page']['chosen'],
              articles_list: data['home_page']['list']
            });
          })
          .catch((error) => {

          });
  }
  // componentDidMount() {
  //   document.addEventListener('scroll', this.trackScrolling);
  // }

  // componentWillUnmount() {
  //   document.removeEventListener('scroll', this.swapp);
  // }



 wheel = (e) => {
    const wrappedElement = document.getElementById('scrollable_Area');
    console.log(e.deltaY);
    if (wrappedElement.getBoundingClientRect().y >= -1 && e.deltaY < 0) {
        window.scrollTo({top: 0, behavior: 'smooth'});
     }
  }

 swapp = (e) => {
    const curStateY = Math.round(document.getElementById('scrollable_Area')
                              .getBoundingClientRect().y)
    if (curStateY == this.state.preStateY && curStateY > -1) {
      window.scrollTo({top: 0, behavior: 'smooth'});
    }
  }

  scroll = (e) => {
    const curStateY = Math.round(document.getElementById('chosen_article')
                              .getBoundingClientRect().y)
    if (e.deltaY < 0) {
        window.scrollTo({top: 0, behavior: 'smooth'});
     }
   }



render() {
  return (
          <div>
                <div className={(!!this.state.is_rtl)?'split right':'split left'} id="chosen_article">
                  <div className="mainCover">
                      {
                        (this.state.chosen_article.cover_url)? <img src={"/media/"+this.state.chosen_article.cover_url} alt="" />:<img src={NoneCover} alt="" />
                      }
                  </div>

                  <div className={"gradientCover"+this.state.is_rtl} onWheel = {(e) => this.scroll(e)} onTouchEnd = {(e) => this.scroll(e)}>
                    <NavLink to={"/"+this.props.lang+'/'+this.state.chosen_article.hashtag} className="titleCover">
                        <p className='CArticle'>FPNDAL'Z BKLUEM[JZ'[Z @ZXLPM</p>
                        <h1>{this.state.chosen_article.title}</h1>
                        <p className={'DArticle'+this.state.is_rtl} dangerouslySetInnerHTML={{ __html: this.state.chosen_article.description }}/>
                    </NavLink>
                  </div>
                </div>

                <div id="scrollUp">
                  <div id="rond"></div>
                </div>

                <div className={(!!this.state.is_rtl)?'split scrollable-element left':'split scrollable-element right'}
                     id="scrollable_section" ref={this.nav} onWheel = {(e) => this.wheel(e)} onTouchEnd = {(e) => this.swapp(e)}>
                  <div className="row" id="scrollable_Area">

                      {
                        this.state.articles_list.map((_,i) => (
                          <ContentBox key={i+1} lang={this.props.lang} data={this.state.articles_list[i]}/>
                        ))
                      }

                  </div>
                </div>
          </div>
    );
  }
}


export default ArticleSection
