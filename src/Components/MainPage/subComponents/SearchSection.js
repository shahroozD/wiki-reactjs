import React, { useState, useRef } from 'react';
import { useTranslation } from "react-i18next";
import { FiSearch } from 'react-icons/fi';
import { FaRegKeyboard } from 'react-icons/fa';

import Header from './Header'

import {is_rtl, setKeyboardLayout} from '../../../utils/Lang/lang'


import Keyboard from 'react-simple-keyboard';
import 'react-simple-keyboard/build/css/index.css';



function SearchSection(props) {


    const [input, setInput] = useState("");
    const [shift, setShift] = useState("default");
    const [layout, setLayout] = useState(setKeyboardLayout(props.lang));
    const [showKB, setShowKB] = useState(0);
    const [sectionHeight, setSectionHeight] = useState(window.innerHeight + "px");


    const submit = (e) => {
      if (input) {
        window.navigator.vibrate(4);
        document.forms[0].submit();
      }
    }

    const dir_lang = (is_rtl(props.lang))?"-rtl":""
    // console.log(is_rtl(props.lang) , dir_lang);
    const keyboard = useRef(null);

    const { t } = useTranslation();


    let onChange = (input) => {
      window.navigator.vibrate(4);
      setInput(input)
      if (shift === "shift") setShift("default");
    }

    let onChangeInput = event => {
      const input = event.target.value;
      setInput(input)
      keyboard.current.setInput(input);
      if (shift === "shift") setShift("default");
    };

    let onKeyPress = (button) => {
      if (button === "{ent}" || button === "{enter}") {
        window.navigator.vibrate(4);
        document.forms[0].submit();
      }
      if (button === "{numbers}" || button === "{abc}") handleNumbers();
      if (button === "{shift}" || button === "{lock}") handleShift();
    }

    let handleShift = () => {
      setShift(shift === "default" ? "shift" : "default")
    };
    let handleNumbers = () => {
      setShift(shift === "numbers" ? "default" : "numbers")
    }


    let showKeyboard = () => {
      setShowKB(showKB ? 0 : 1)
    }



    return(

      <div className="search_section" style={{height: sectionHeight}}>
      <Header lang={props.lang}/>
          <form action={"/"+props.lang+"/search"}>
            <fieldset>
              <legend>{t("mainSearchTitle")}</legend>
              <div className="inner-form">
                <div className="input-field">
                <button type="button" className={"btn-keyboard btn-keyboard-dir"+dir_lang } onClick={showKeyboard}>
                    <FaRegKeyboard style={{ fill: (showKB)? '#0303b0' : '#6a6969'}}/>
                </button>
                  <div className="choices" data-type="text" aria-haspopup="true" aria-expanded="false">
                    <div className={"choices__inner choices__inner-dir"+dir_lang}>

                      <input value={input} onChange={onChangeInput} type="text" name="q"
                             id="q" className="choices__input choices__input--cloned"
                             placeholder={t('mainSerchPlaceholder')} autoComplete="off"/>
                    </div>

                  </div>

                  <button type="button" onClick={submit}
                          className={"btn-search btn-search-dir"+dir_lang}>
                      <FiSearch />
                  </button>


                </div>
              </div>
          { /*   <Keyboard
                keyboardRef={r => (keyboard.current = r)}
                onChange={onChange}
                onKeyPress={onKeyPress}
                layoutName={shift}
                layout={layout}
              /> */}

              <div className={'keyboard_div '+ (!showKB &&'keyboard_hide')}>
                  <Keyboard
                    keyboardRef={r => (keyboard.current = r)}
                    onChange={onChange}
                    onKeyPress={onKeyPress}
                    theme= {'hg-theme-default hg-layout-default '+(showKB &&'simple-keyboard_show')}
                    layoutName={shift}
                    mergeDisplay= {true}
                    layout= {layout}
                      display= {{
                        "{numbers}": "123",
                        "{ent}": t('KeyboardEnt'),
                        "{backspace}": "⌫",
                        "{shift}": "⇧",
                        "{abc}": t("KeyboardABC")
                      }}
                  />
              </div>

            </fieldset>
          </form>



          <div id="scrollDown">
            <div id="rond"></div>
          </div>


      </div>

    );

}
export default SearchSection
