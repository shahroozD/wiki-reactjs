import React, {useState, useRef} from 'react';
import { useHistory, Redirect } from 'react-router-dom'
import { useTranslation } from "react-i18next";
import { FiSearch } from 'react-icons/fi';
import { FiGlobe } from "react-icons/fi";

import Keyboard from 'react-simple-keyboard';
import 'react-simple-keyboard/build/css/index.css';

import {is_rtl, is_unicode, lang_list} from '../../../utils/Lang/lang'

function Header(props) {
    let { t } = useTranslation();
    let history = useHistory();

    const [showLangModal, setShowLangModal] = useState(false);
    const [dir_lang, setDir_lang] = useState((is_rtl(props.lang))?"-rtl":"");
    var langs = [];
    let isUni
    for (const [key, value] of Object.entries(lang_list())) {
        isUni = (is_unicode(key))?" is_unicode":""
        langs.push(<div key={key} className={'lang_link ' + (key == props.lang && 'link_selected ')  + isUni } onClick={ () => changeLang(key)}>{value.showName}</div>);
    }



    const show_modal = () => {
      setShowLangModal(!showLangModal)
    }
    const changeLang = (lang) => {
      if (props.lang != lang) {
        window.location.href = "/"+lang
      }
      else {
        setShowLangModal(!showLangModal)
      }
    }




    return (
          <div className={'mainHeade'}>

          <div id="myModal" className={"modal " + (!showLangModal &&'hidden_modal')} >
            <div className="closeModal" onClick={show_modal}></div>
            <div className="modal-content">
                <div className="modal-header">
                    <button className="close" onClick={show_modal}>*</button>
                </div>
                {langs}
            </div>
          </div>

              <div className={'mainHeadeLogo'}>
                  R[JZNDA
              </div>
              <button className={'mainHeadeLang'+dir_lang} onClick={show_modal}>
                  <FiGlobe className=''/>
              </button>
          </div>
    );
}


export default Header
