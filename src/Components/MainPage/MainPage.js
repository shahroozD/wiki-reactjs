import React, { useState, useRef, useEffect } from 'react';
import { useTranslation } from "react-i18next";
import i18next from 'i18next';
// import "fullpage.js/vendors/scrolloverflow"; // Optional. When using scrollOverflow:true
// import ReactFullpage from "@fullpage/react-fullpage";
import SearchSection from './subComponents/SearchSection'
import ArticleSection from './subComponents/ArticleSection'
import {is_rtl, is_unicode} from '../../utils/Lang/lang'
// import Article from './Article'
// import Footer from './Footer'
// import 'normalize.css/normalize.css'
import '../../styles/mainPageStyle/style.scss';




function MainPage(props) {

  const [initialClientY, setInitialClientY] = useState(0);
  const [finalClientY, setFinalClientY] = useState(500);

  const section = useRef(null)
  let current_lang = i18next.language
  const { t, i18n } = useTranslation();

    // document.body.addEventListener('scroll', scroll, true);

  useEffect(()=>{
    document.body.style.overflow = "hidden"
    // section.current.style.overflow = "hidden"
    // section.current.style.height = window.innerHeight + "px";
    if (is_unicode(current_lang)) {
      document.body.style.fontFamily = ['ardeshir', 'roboto', 'sahel'];
    }

    if (is_rtl(current_lang)) {
      document.body.style.direction = "rtl";
    }
    if (is_unicode(current_lang)) {
      document.body.style.fontFamily = ['ardeshir', 'roboto'];
    }
  },[])


  if (!!props.match.params.lang) {
    if (current_lang != props.match.params.lang) {
      current_lang = props.match.params.lang
      i18n.changeLanguage(props.match.params.lang);
    }
  }else {

  }





  const wheel = (e) => {
     const wrappedElement = document.body;
     if (e.deltaY > 0) {
         window.scrollTo({top: document.body.scrollHeight, behavior: 'smooth'});
      }
   }

   const handleTouchStart = (e) => {
        setInitialClientY(e.nativeEvent.touches[0].clientY)
    }

    const handleTouchMove = (event) => {
        setFinalClientY(event.nativeEvent.touches[0].clientY);
    }

    const swapp = (e) => {
        console.log(initialClientY-finalClientY);
        if (finalClientY < initialClientY & (initialClientY-finalClientY > 200)) {
          window.scrollTo({top: document.body.scrollHeight, behavior: 'smooth'});
          console.log('swipe up')
        } else{
          console.log(initialClientY-finalClientY);

          console.log('swipe down')
        }
        setFinalClientY(500)
    }






    // let is_rtl = (current_lang == 'uCLC')?"-rtl":""




    return (

      <div className="main_page" onWheel = {(e) => wheel(e)} onTouchEnd = {(e) => swapp(e)}
           onTouchStart = {(e) => handleTouchStart(e)} onTouchMove={(e)=>handleTouchMove(e)}>

          <div className="section fp-auto-height-responsive" id="section0" style={{zIndex:10}}>
                <SearchSection lang={current_lang}/>
          </div>


          <div  className="section" id="section2">


              <ArticleSection lang={current_lang}/>


          </div>
      </div>


    );

}


export default MainPage
