const path = require('path');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.join(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [{
      loader: 'babel-loader',
      test: /\.js$/,
      exclude: /node_modules/
    }, {
      test: /\.s?css$/,
      use: [
        'style-loader',
        'css-loader',
        'sass-loader'
      ]
    }, {
      test: /\.(jpe?g|png|gif|svg)$/i,
      loader: "file-loader?name=/img/[name].[ext]"
    },
    {
      test: /\.(eot|svg|ttf|woff|woff2)$/,
      loader: 'file-loader?name=/fonts/[name].[ext]'
    }]
  },
  devtool: 'cheap-module-eval-source-map',
  devServer: {
    port: '8002',
    host: '0.0.0.0',
    proxy: {
      '/api': {
         target: {
            host: "192.168.1.8",
            protocol: 'http:',
            port: 8001
         },
         pathRewrite: {
            '^/api': '/api'
         }
      },
      '/media': {
         target: {
            host: "192.168.1.8",
            protocol: 'http:',
            port: 8001
         },
         pathRewrite: {
            '^/media': '/media'
         }
      },
    },
    historyApiFallback: true,
    contentBase: path.join(__dirname, 'public')
  },
  mode: 'development'
};
